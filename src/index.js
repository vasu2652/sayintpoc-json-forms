
import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { Actions, jsonformsReducer, NOT_APPLICABLE } from '@jsonforms/core';
import { materialFields, materialRenderers } from '@jsonforms/material-renderers';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import BooleanControlTester from './customTesters/BooleanControlTester';
import DivisionControlTester from './customTesters/DivisionControlTester';
import BooleanControl from './customRenderers/BooleanControl';
import SearchControlTester from './customTesters/searchControlTester';
import SearchControl from './customRenderers/searchControl';
import dateControl from './customRenderers/dateControl';
import dateControlTester from './customTesters/dateControlTester';
import TextAreaControlTester from './customTesters/textAreaControlTester';
import TextAreaControl from './customRenderers/textAreaControl';
import DivisionControl from './customRenderers/divisionControl';
import TextInputControlTester from './customTesters/textControlTester';
import TextInputControl from './customRenderers/textControl';
import SelectControl from './customRenderers/selectControl';
import SelectControlTester from './customTesters/selectControlTester';
import ColorPickerControl from './customRenderers/colorPickerControl';
import ColorPickerControlTester from './customTesters/ColorPickerControlTester';
import IconPickerControl from './customRenderers/iconPickerControl';
import IconPickerControlTester from './customTesters/iconPickerControlTester';
// import SelectMultipleControlTester from './customTesters/selectMultipleControlTester';
// import SelectMultipleControl from './customRenderers/selectMultipleControl';
// import uischema from './uischema.json';
// import schema from './jsonschema.json';
import uischema from './caasUiSchema.json';
import uiSchemaFunc from './functions/uiSchema';
import schema from './caasJsonSchema.json';
// var uischema=uiSchemaFunc.getFormComponent(0);
// console.log(uischema);
var data={

};
const store = createStore(
	combineReducers({ jsonforms: jsonformsReducer() }),  
	{
	  jsonforms: {
		renderers: materialRenderers,
		fields: materialFields,
	  }
	}
  );
store.dispatch(Actions.init(data, schema, uischema));
// store.dispatch(Actions.registerRenderer(SearchControlTester, SearchControl));
// store.dispatch(Actions.registerRenderer(SelectControlTester, SelectControl));
store.dispatch(Actions.registerRenderer(BooleanControlTester, BooleanControl));
store.dispatch(Actions.registerRenderer(ColorPickerControlTester, ColorPickerControl));
store.dispatch(Actions.registerRenderer(IconPickerControlTester, IconPickerControl));
// store.dispatch(Actions.registerRenderer(dateControlTester, dateControl));
 store.dispatch(Actions.registerRenderer(TextAreaControlTester, TextAreaControl));
 store.dispatch(Actions.registerRenderer(DivisionControlTester, DivisionControl));
 store.dispatch(Actions.registerRenderer(TextInputControlTester, TextInputControl));
 console.log(TextInputControlTester);

// store.dispatch(Actions.registerRenderer(SelectMultipleControlTester, SelectMultipleControl));
// store.dispatch(Actions.regis)
ReactDOM.render(
<Provider store={store}>
	<App />
</Provider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
