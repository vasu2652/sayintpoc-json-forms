import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  ColorPickerSelect  from '../components/colorPicker';
const ColorPickerControl = (props) => {
  console.log("color",props);
	return <ColorPickerSelect
  {...props.uischema}
  value={props.data}
  label={props.label}
  onClick={ev => props.handleChange(props.path, ev.value)}
  />
};

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(ColorPickerControl);