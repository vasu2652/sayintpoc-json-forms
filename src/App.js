import React, { Component } from 'react';
import { connect } from 'react-redux';
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import logo from './logo.svg';
import './App.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { JsonForms } from '@jsonforms/react'
import { getData, JsonFormsState } from '@jsonforms/core';
import "react-responsive-carousel/lib/styles/carousel.min.css";
//import { Carousel } from 'react-responsive-carousel';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  async handleClick() {
    var complianceData = JSON.parse(this.props.dataAsString);
    var compliance_data = [];
    var requestObject = {};
    Object.keys(complianceData).forEach(function (key) {
      compliance_data.push(complianceData[key]);
    });
    requestObject.compliance_data = [...compliance_data];
    var res1 = await fetch("http://172.16.6.135/api/conversations/1/0?data=true").then(data => data.json());
    var agent_text = res1.data[0].data.transcription.agent_text;
    requestObject.agent_object = [...agent_text];
    var res2 = await fetch("http://172.16.6.95:3718/nlp_custom_compliance", {
      method: 'post',
      body: JSON.stringify(requestObject)
    }).then(res => res.json());
    console.log(res2);
    // .then(response=>{
    //   var agent_text=response.data[0].data.transcription.agent_text;
    //   requestObject.agent_text=[...agent_text];
    // });
  }
  render() {
    return (
      // <div className="App"  style={{textAlign:'left',width:"100%",margin:'auto'}} >
      //   {/* <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <p>
      //       Edit <code>src/App.js</code> and save to reload.
      //     </p>
      //     <a
      //       className="App-link"
      //       href="https://reactjs.org"
      //       target="_blank"
      //       rel="noopener noreferrer"
      //     >
      //       Learn React
      //     </a>
      //   </header> */}
      //   <JsonForms/>
      //   <button onClick={this.handleClick}>submit</button>
      //   <div>
      //         <pre>{this.props.dataAsString}</pre>
      //       </div>
      // </div>
      // <Carousel showThumbs={false} useKeyboardArrows={false}>
      // <div style={{height:'100%'}}>
      // </div>
      // <div style={{height:'100%'}}>
      //   <JsonForms/>
      // </div>
      // </Carousel>
      <JsonForms/>
    );
  }
}

const mapStateToProps = (state: JsonFormsState) => {
  return { dataAsString: JSON.stringify(getData(state), null, 2) }
};
export default connect(mapStateToProps)(withStyles()(App));
