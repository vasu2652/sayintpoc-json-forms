import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, withTheme, createMuiTheme } from '@material-ui/core/styles';
const styles = theme => ({
  root: {
    // padding: theme.spacing.unit * 2,
    float:'right',
    paddingTop: 0,
    //border: '1px solid #f2f2f2',
    // Match <Inspector /> default theme.
    backgroundColor: theme.palette.type === 'light' ? theme.palette.common.white : '#242424',
    //minHeight: theme.spacing.unit * 40,
    // width: '100%',
    // margin: 10,
    width: '100%',
    left: 10,
    position: 'relative'

  }
});

class Division extends React.Component {
  state = {
  };

  render() {
    const { classes,label, style } = this.props;
    const { checked } = this.state;
    console.log(style)

    return (
      <div className={classes.root} style={style}>{label}
      </div>
    );
  }
}

Division.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(withTheme()(Division));
