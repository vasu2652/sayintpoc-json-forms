import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  IconPicker  from '../components/iconPicker';
const iconPickerControl = (props) => {
  // console.log(props);
	return <IconPicker
    value={props.data}
    label={props.label}
    style={props.uischema.style}
    onClick={ev => props.handleChange(props.path, ev.value)}
  />
};

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(iconPickerControl);