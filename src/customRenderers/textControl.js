import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  TextInput  from '../components/textInput';

const TextControl = ({ data, handleChange, path,label }) => (
  <TextInput
    value={data}
    label={label}
    onClick={ev => handleChange(path, (ev.value))}
  />
);

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(TextControl);