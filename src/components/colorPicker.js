import React from 'react';
import url from 'url';
import PropTypes from 'prop-types';
import { withStyles, withTheme, createMuiTheme } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { compose } from 'redux';
import ColorPicker from 'material-ui-color-picker';
import FormHelperText from '@material-ui/core/FormHelperText';


class ColorPickerSelect extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    color: '#000',
  };

  handleChange = (name, onClick) => event => {
    this.setState({ [name]: event }, () =>
      onClick({ value: event }));
  };

  render() {
    const { classes, value, onClick, label } = this.props;
    const { color } = this.state;

    return (
      <>
        <ColorPicker
          name='color'
          defaultValue='#000'
          value={this.state.color}
          onChange={this.handleChange('color', onClick)}
        />
        <FormHelperText id="component-helper-text">{this.props.description}</FormHelperText>
      </>
    );
  }
}

ColorPickerSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default compose(
  withTheme()
)(ColorPickerSelect);
