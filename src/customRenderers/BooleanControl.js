import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  Boolean  from '../components/Boolean';
const BooleanControl = (props) => {
  // console.log(props);
	return <Boolean
    value={props.data}
    label={props.label}
    style={props.uischema.style}
    onClick={ev => props.handleChange(props.path, ev.value)}
  />
};

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(BooleanControl);