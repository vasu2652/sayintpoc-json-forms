import * as React from 'react';
import { mapStateToControlProps, mapDispatchToControlProps } from '@jsonforms/core';
import { connect } from 'react-redux';
import  Search  from '../components/materialSearch';

const SearchControl = ({ data, handleChange, path,label }) => {
  return <Search
    value={data}
    label={label}
    onClick={ev => handleChange(path, ev.value)}
  />
}
  

export default connect(
  mapStateToControlProps,
  mapDispatchToControlProps,
)(SearchControl);