import React from 'react';
import MaterialUiIconPicker from 'react-material-ui-icon-picker';
 
class IconPicker extends React.Component {
    
    showPickedIcon = (icon) => {
        console.info(icon) // prints {name: "access_alarm", code: "e190"}
    }
    
    render() {
        return (
            <MaterialUiIconPicker className="material-icon-outlined" onPick={this.showPickedIcon} />
        )
    }
}
export default IconPicker;
